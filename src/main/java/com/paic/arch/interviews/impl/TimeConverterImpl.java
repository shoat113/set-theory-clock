package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeConverterImpl implements TimeConverter {
    private Logger logger = LoggerFactory.getLogger(TimeConverterImpl.class);

    @Override
    public String convertTime(String aTime) {
        String result="";
        String second = aTime.substring(6,8);
        String hour = aTime.substring(0,2);
        String minite = aTime.substring(3,5);

        result = getSecondLight(second)+"\n"+getHourLight(hour)+"\n"+getMiniteLight(minite);

        return result;
    }

    /**
     * 获得秒状态
     *
     * @param strSecond 秒
     * @return 黄灯显示状态
     */
    private String getSecondLight(String strSecond) {
        int second;
        try {
            // 格式化秒
            second = Integer.parseInt(strSecond);
        } catch (Exception e) {
            logger.error("处理数字异常");
            return "";
        }

        // 秒对2取余，余数为1，则显示0，否则显示Y
        return second % 2 == 1 ? "O" : "Y";
    }

    /**
     * 获得小时状态
     *
     * @param strHour 小时
     * @return 小时显示状态
     */
    private String getHourLight(String strHour) {
        String firstLineResult = "OOOO";
        String secondLineResult = "OOOO";
        int hour;
        try {
            // 格式化秒
            hour = Integer.parseInt(strHour);
        } catch (Exception e) {
            logger.error("处理数字异常");
            return "";
        }
        // 处理第一排，第一排单位是5，获得除五的整数部分即可
        int firstLineNum = hour / 5;
        for (int i = 0; i < firstLineNum; i++) {
            firstLineResult = firstLineResult.replaceFirst("O", "R");
        }
        // 处理第二排，总数减去第一排总数，剩余的部分就是第二排
        int secondLineNum = hour - firstLineNum * 5;
        for (int i = 0; i < secondLineNum; i++) {
            secondLineResult = secondLineResult.replaceFirst("O", "R");
        }
        return firstLineResult + "\n" + secondLineResult;
    }

    /**
     * 获得分钟状态
     *
     * @param strMinite 分钟
     * @return 分钟显示状态
     */
    private String getMiniteLight(String strMinite) {
        String firstLineResult = "OOOOOOOOOOO";
        String secondLineResult = "OOOO";
        int minite;
        try {
            // 格式化分钟
            minite = Integer.parseInt(strMinite);
        } catch (Exception e) {
            logger.error("处理数字异常");
            return "";
        }
        // 处理第一排，第一排单位是5，获得除五的整数部分即可
        int firstLineNum = minite / 5;
        for (int i = 0; i < firstLineNum; i++) {
            if (i == 2 || i == 5 || i == 8) {
                firstLineResult = firstLineResult.replaceFirst("O", "R");
            } else
                firstLineResult = firstLineResult.replaceFirst("O", "Y");
        }
        // 处理第二排，总数减去第一排总数，剩余的部分就是第二排
        int secondLineNum = minite - firstLineNum * 5;
        for (int i = 0; i < secondLineNum; i++) {
            secondLineResult = secondLineResult.replaceFirst("O", "Y");
        }
        return firstLineResult + "\n" + secondLineResult;
    }

    public static void main(String args[]) {
        TimeConverterImpl t = new TimeConverterImpl();
        System.out.println(t.convertTime("23:59:59"));
    }
}
